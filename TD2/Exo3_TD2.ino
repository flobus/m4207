void setup() {
  // put your setup code here, to run once:
  pinMode(A0, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int valeur=analogRead(A0);    /* L'étendue des valeurs est de 0 à 1023*/
  Serial.println(valeur);
  delay(100);
}
